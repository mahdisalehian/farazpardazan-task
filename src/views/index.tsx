import { useState, useEffect } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import TableFilter from "components/tableFilter";
import Table from "components/table";
import { Data, HeadCell } from "components/table/types";
import { Queries } from "components/tableFilter/types";
import { filterTableByQueries } from "utils/tableUtils";
import { parseQueryParams } from "utils/common";

const useStyles = makeStyles({
  cardOuter: {
    padding: 50,
  },
});

const headCells: HeadCell[] = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Name",
  },
  { id: "date", numeric: false, disablePadding: true, label: "Changed Date" },
  { id: "title", numeric: false, disablePadding: true, label: "Title" },
  { id: "field", numeric: false, disablePadding: true, label: "Field" },
  { id: "old_value", numeric: false, disablePadding: true, label: "Old Value" },
  { id: "new_value", numeric: false, disablePadding: true, label: "New Value" },
];

export default function MainPage() {
  const [loading, setLoading] = useState(false);
  const [queries, setQueries] = useState<Queries>({});
  const [usersActions, setUsersActions] = useState<Data[]>([]);
  const [filteredUsersActions, setFilteredUsersActions] = useState<Data[]>([]);

  const getUsersActions = async () => {
    try {
      setLoading(true);
      const { data } = await axios.get<Data[]>("./data.json");
      setUsersActions(data);
      setFilteredUsersActions(data);
      setQueries(parseQueryParams(window.location.search));
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    getUsersActions();
  }, []);

  useEffect(() => {
    setFilteredUsersActions(
      filterTableByQueries(usersActions, queries) as Data[]
    );
  }, [queries]); // eslint-disable-line react-hooks/exhaustive-deps

  const classes = useStyles();
  return (
    <div className={classes.cardOuter}>
      <TableFilter setQueries={setQueries} />
      <Table
        headCells={headCells}
        rows={filteredUsersActions}
        defaultSorted="date"
        loading={loading}
      />
    </div>
  );
}
