import { ThemeOptions, Theme } from "@material-ui/core/styles";

export default function getOverrides(theme: Theme): ThemeOptions["overrides"] {
  return {
    MuiCssBaseline: {
      "@global": {
        body: {
          backgroundColor: theme.palette.grey[100],
          fontFamily: "IRANSans, sans-serif",
        },
      },
    },
    MuiTableCell: {
      root: {
        fontFamily: "inherit",
      },
    },
    MuiFormControl: {
      marginNormal: {
        marginBottom: 16,
      },
    },
  };
}
