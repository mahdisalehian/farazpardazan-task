import { unstable_createMuiStrictModeTheme as createMuiTheme } from "@material-ui/core";
import getOverrides from "./overrides";

const theme = createMuiTheme();

theme.overrides = getOverrides(theme);

export default theme;
