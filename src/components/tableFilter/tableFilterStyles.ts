import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useTableStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      width: "100%",
      boxShadow: "none",
      marginBottom: 50,
      padding: 24,
    },
  })
);

export default useTableStyles;
