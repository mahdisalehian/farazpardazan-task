export interface FormFields {
  name: string;
  date: string | null;
  title: string;
  field: string;
}

export interface Queries {
  name?: string;
  date?: string;
  title?: string;
  field?: string;
}