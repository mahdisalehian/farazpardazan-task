import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { useForm, Controller } from "react-hook-form";
import useTableFilterStyles from "./tableFilterStyles";
import Button from "@material-ui/core/Button";
import { parseQueryParams } from "utils/common";
import {
  convertDateToYYYYMMDD,
  deleteFalsyKey,
  objectToQueryString,
} from "utils/common";
import { FormFields, Queries } from "./types";

interface TableFilterProps {
  setQueries: React.Dispatch<React.SetStateAction<Queries>>;
}

const TableFilter = ({ setQueries }: TableFilterProps) => {
  const classes = useTableFilterStyles();
  const queries: Queries = parseQueryParams(window.location.search);
  const { register, handleSubmit, control } = useForm<FormFields>({defaultValues:{
    date: queries.date || null
  }});
  const onSubmit = handleSubmit(({ date, name, field, title }) => {
    const queries = {
      date: convertDateToYYYYMMDD(date),
      name,
      field,
      title,
    };
    setQueries((prevQueries) => {
      const newQueries = deleteFalsyKey({ ...prevQueries, ...queries });
      window.history.pushState({}, "", `/${objectToQueryString(newQueries)}`);
      return newQueries;
    });
  });
  return (
    <Paper className={classes.paper}>
      <Grid component="form" onSubmit={onSubmit} container spacing={1}>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            fullWidth
            size="small"
            label="Name"
            variant="outlined"
            defaultValue={queries.name}
            {...register("name")}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Controller
              control={control}
              name="date"
              render={({ field: { onChange, onBlur, value, name, ref } }) => (
                <DatePicker
                  clearable
                  margin="none"
                  label="Change Date"
                  format="yyyy-MM-dd"
                  views={["year", "month", "date"]}
                  disableFuture
                  size="small"
                  fullWidth
                  inputVariant="outlined"
                  {...{ onChange, onBlur, value, name }}
                  inputRef={ref}
                />
              )}
            />
          </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            fullWidth
            size="small"
            label="Title"
            variant="outlined"
            defaultValue={queries.title}
            {...register("title")}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            fullWidth
            size="small"
            label="Field"
            variant="outlined"
            defaultValue={queries.field}
            {...register("field")}
          />
        </Grid>
        <Grid item sm={12}>
          <Button variant="contained" color="primary" type="submit">
            Search
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default TableFilter;