import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import useTableToolbarStyles from './tableToolbarStyles';

interface TableToolbarProps {
  numSelected: number;
}

const TableToolbar = (props: TableToolbarProps) => {
  const classes = useTableToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          className={classes.title}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Users Actions
        </Typography>
      )}
    </Toolbar>
  );
};

export default TableToolbar;
