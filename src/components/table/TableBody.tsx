import TableCell from "@material-ui/core/TableCell";
import MUITableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import { stableSort, getComparator } from "utils/tableUtils";
import { Order } from "types/common";
import { Data } from "./types";

interface EnhancedTableProps {
  rows: Data[];
  order: Order;
  orderBy: string;
  page: number;
  rowsPerPage: number;
  selected: number[];
  setSelected: React.Dispatch<React.SetStateAction<number[]>>;
}

const TableBody = ({
  rows,
  order,
  orderBy,
  page,
  rowsPerPage,
  selected,
  setSelected,
}: EnhancedTableProps) => {
  const isSelected = (id: number) => selected.indexOf(id) !== -1;
  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const handleClick = (
    event: React.ChangeEvent<HTMLInputElement>,
    id: number
  ) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: number[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
    localStorage.setItem("selectedIds", newSelected.join(","));
  };

  return (
    <MUITableBody>
      {stableSort<any>(rows, getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row, index) => {
          const isItemSelected = isSelected(row.id);
          const labelId = `enhanced-table-checkbox-${index}`;

          return (
            <TableRow tabIndex={-1} key={row.id}>
              <TableCell padding="checkbox" align="center">
                <Checkbox
                  checked={isItemSelected}
                  onChange={(event) => handleClick(event, row.id)}
                  inputProps={{ "aria-labelledby": labelId }}
                />
              </TableCell>
              <TableCell
                component="th"
                id={labelId}
                scope="row"
                padding="none"
                align="center"
              >
                {row.name}
              </TableCell>
              <TableCell align="center">{row.date}</TableCell>
              <TableCell align="center">{row.title}</TableCell>
              <TableCell align="center">{row.field}</TableCell>
              <TableCell align="center">{row.old_value}</TableCell>
              <TableCell align="center">{row.new_value}</TableCell>
            </TableRow>
          );
        })}
      {emptyRows > 0 && (
        <TableRow style={{ height: 53 * emptyRows }}>
          <TableCell colSpan={6} />
        </TableRow>
      )}
    </MUITableBody>
  );
};

export default TableBody;
