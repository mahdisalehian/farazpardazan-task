import { useState, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import Paper from "@material-ui/core/Paper";
import TableHeade from "./TableHeade";
import TableBody from "./TableBody";
import useTableStyles from "./tableStyles";
import { Order } from "types/common";
import { Data, HeadCell } from "./types";
import CircularProgress from "@material-ui/core/CircularProgress";
import clsx from "clsx";

interface TableProps {
  rows: Data[];
  headCells: HeadCell[];
  defaultSorted?: keyof Data;
  loading: boolean;
}

export default function EnhancedTable({
  rows,
  headCells,
  defaultSorted,
  loading,
}: TableProps) {
  defaultSorted = defaultSorted
    ? defaultSorted
    : (Object.keys(rows[0])[0] as keyof Data);

  const classes = useTableStyles();
  const [order, setOrder] = useState<Order>("asc");
  const [orderBy, setOrderBy] = useState<keyof Data>(defaultSorted);
  const [selected, setSelected] = useState<number[]>(
    localStorage
      .getItem("selectedIds")
      ?.split(",")
      .map((id) => +id) || []
  );
  const [page, setPage] = useState(0);
  const rowsPerPage = Math.min(rows.length, 20);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  useEffect(() => {
    setOrderBy(defaultSorted as keyof Data);
  }, [rows, defaultSorted]);

  return (
    <div>
      <Paper className={clsx(classes.paper, loading && classes.tableIsloading)}>
        {loading ? (
          <CircularProgress color="secondary" />
        ) : (
          <>
            <TableContainer>
              <Table
                className={classes.table}
                aria-labelledby="tableTitle"
                size="medium"
                aria-label="enhanced table"
              >
                <TableHeade
                  headCells={headCells}
                  order={order}
                  orderBy={orderBy}
                  onRequestSort={handleRequestSort}
                />
                <TableBody
                  order={order}
                  orderBy={orderBy}
                  page={page}
                  rows={rows}
                  rowsPerPage={rowsPerPage}
                  selected={selected}
                  setSelected={setSelected}
                />
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
            />
          </>
        )}
      </Paper>
    </div>
  );
}
