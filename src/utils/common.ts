export const convertDateToYYYYMMDD = (date: string | undefined | null) => {
  const _date = date ? new Date(date) : undefined;
  if (_date) return _date.toISOString().split("T")[0];
  else return undefined;
};

export const deleteFalsyKey = (obj: { [key in string | number]: any }) => {
  Object.keys(obj).forEach(
    (k) =>
      (obj[k] === null || obj[k] === undefined || obj[k] === "") &&
      delete obj[k]
  );
  return obj;
};

export const objectToQueryString = (obj: { [key in string | number]: any }) => {
  if (!Object.keys(obj).length) return "";
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return "?" + str.join("&");
};

export const parseQueryParams = (querystring: string) => {
  const search = querystring.substring(1);
  return search
    ? JSON.parse(
        '{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function (key, value) {
          return key === "" ? value : decodeURIComponent(value);
        }
      )
    : {};
};
