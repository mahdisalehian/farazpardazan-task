import { Order } from "types/common";

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

export function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

export function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const leftMost = (
  data: any[],
  date: string,
  low = 0,
  high = data.length - 1,
  result = -1,
  firstResult: null | number = null
): any => {
  if (low <= high) {
    const mid = Math.ceil((low + high) / 2);
    if (date === data[mid].date) {
      if (!firstResult) firstResult = mid;
      result = mid;
      return leftMost(data, date, low, mid - 1, result, firstResult);
    } else if (date < data[mid].date) {
      return leftMost(data, date, low, mid - 1, result, firstResult);
    } else {
      return leftMost(data, date, mid + 1, high, result, firstResult);
    }
  }
  return [result, firstResult];
};

const rightMost = (
  data: any[],
  date: string,
  low = 0,
  high = data.length - 1,
  result = -1
): any => {
  if (low <= high) {
    const mid = Math.ceil((low + high) / 2);
    if (date === data[mid].date) {
      result = mid;
      return rightMost(data, date, mid + 1, high, result);
    }

    if (date < data[mid].date) {
      return rightMost(data, date, low, mid - 1, result);
    } else {
      return rightMost(data, date, mid + 1, high, result);
    }
  }
  return result;
};

function filterDate(data: any[], date: string) {
  const orderedData = data.sort(
    (a, b) => +a.date.split("-").join("") - +b.date.split("-").join("")
  );
  const [firstIndex, firstResult] = leftMost(orderedData, date);
  const lastIndex = rightMost(orderedData, date, firstResult);
  return data.slice(firstIndex, lastIndex + 1);
}

export function filterTableByQueries(
  data: Array<{ [key in number | string]: any }>,
  queries: { [key in number | string]: any }
) {
  const { date, ...restQueries } = queries;
  let filteredData = [...data];
  if (date) {
    filteredData = filterDate(filteredData, date);
  }
  if (filteredData.length && Object.keys(restQueries).length) {
    filteredData = filteredData.filter((rowData) => {
      for (const key in restQueries) {
        if (
          !rowData[key]
            .toLocaleLowerCase()
            .includes(queries[key]!.toLocaleLowerCase())
        )
          return false;
      }
      return true;
    });
  }
  return filteredData;
}
