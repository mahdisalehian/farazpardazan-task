import { lazy, Suspense } from "react";
import { Route, Switch, Redirect, BrowserRouter } from "react-router-dom";

const MainPage = lazy(() => import("./views"));

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact render={() => <MainPage />} />
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
